<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

$app = app('Dingo\Api\Routing\Router');
$app->version('v1', [
    'namespace' => 'App\Http\Controllers\Api'
], function ($app) {
    $app->post('/register', 'RegisterController@register');
    $app->post('/session', 'SessionController@login');

    $app->group(['middleware' => 'api.auth'], function ($app) {
        $app->get('/session', 'SessionController@show');
        $app->delete('/session', 'SessionController@logout');
        $app->put('/session', 'SessionController@refresh');
    });

    $app->post('article', 'ArticleController@store');
    $app->delete('article/{id}', 'ArticleController@destroy');
    $app->put('article/{id}', 'ArticleController@update');
    $app->get('article/{id}', 'ArticleController@index');
});
