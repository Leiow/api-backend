import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      name: 'articles',
      path: '/index',
      component: resolve => {
        require(['./Articles/'], resolve);
      },
    },
    {
      name: 'about',
      path: '/about',
      component: resolve => {
        require(['./About/'], resolve);
      },
    },
    {
      name: 'admin',
      path: '/admin',
      component: resolve => {
        require(['./Admin'], resolve);
      },
      children: [
        {
          name: 'dashboard',
          path: 'dashboard',
          component: resolve => {
            require(['./Admin/Dashboard'], resolve);
          },
        },
        {
          name: 'editor',
          path: 'editor',
          component: resolve => {
            require(['./Admin/Editor'], resolve);
          },
        },
      ],
    },
  ],
});