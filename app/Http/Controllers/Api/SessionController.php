<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Dingo\Api\Routing\Helpers;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{

    use AuthenticatesUsers;
    use Helpers;

    public function show(Request $request)
    {
        return auth()->user();
    }

    public function login(Request $request)
    {
        if ($token = Auth::guard('api')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ])) {
            return $this->sendLoginResponse($request, $token);
        } else {
            return $this->sendFailedLoginResponse();
        }
    }

    public function sendLoginResponse(Request $request, $token)
    {
        $this->clearLoginAttempts($request);
        return $this->authenticated($token);
    }

    public function authenticated($token)
    {
        return $this->response->array([
            'token' => $token
        ]);
    }

    public function sendFailedLoginResponse()
    {
        throw new UnauthorizedHttpException('Bad Credentials');
    }

    public function logout(Request $request)
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return $this->response->noContent();
    }

    public function refresh()
    {
        return $this->response->array([
            'token' => JWTAuth::refresh()
        ]);
    }
}
