
# Public API Reference Guide

## Overview

This reference guide describes the public APIs about this projection. Use RESTful API commands to describe in this guide.

All API URIs in this guide use the common prefix `http://localhost:port/api`.


## Common Request Header

The following parameters are required in the  HTTP header of all API request, except for the login api.

| Parameter | Value |
| -- | -- |
| Accept | application/{standards tree}.{project name}.{api version}+json |
| Authorization | Bearer {token} |

To call the APIs, exept `register` and `login`, you must use `Authorization` in HTTP header.

1. Standards Tree

There are three different trees: `x`, `prs` and `vnd`. To use which tree is depend upon your project which you are developing.

- `x` is unregisterd tree. It is primarily meant for local or private project.
- `prs` is personal tree. It is primarily meant for the private project that are not distributed commercially.
- `vnd` is the vendor tree. It is primarily meant for the project that publicially availlible and distributed.

2. Project Name

This is your project name or your domain name.

3. API Version

This version is your default API version and is used whenever is not supplied.


## Login Session

### Register

- Request URI

> /register

- Request Method

> POST

- Request Parameters

| Name | Type | Required | Description |
| -- | -- | -- | -- |
| email | String | Y | User's email |
| password | String | Y | Logon password |

- Response (201)

No response message.

### Login

Use this API, you can get the credential, `token`, for other API.

- Request URI

> /session

- Request Method

> POST

- Request Parameters

| Name | Type | Required | Description |
| -- | -- | -- | -- |
| email | String | Y | Logon user's email |
| password | String | Y | User's password |

- Response (200)

| Name | Type | Description |
| -- | -- | -- |
| token | String | Access token |


### Refresh

Each `token` has expiration time, and the default live time is `600` seconds. When the `token` is invalid, all APIs will return invalid token errors.

- Request URI

> /session

- Request Method

> PUT

- Request Parameters

No requrest parameters.

- Response (200)

| Name | Type | Description |
| -- | -- | -- |
| token | String | Access token |

### Logout

When authorized user logout, use this api, then the token will be invalided.

- Request URI

> /session

- Request Method

> DELETE

- Request Parameters

No request parameters.

- Response (204)

No Response message.




